/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 * Not a Contribution.
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "GattIncludedService.hpp"

using namespace std;
namespace gatt{

GattIncludedService::GattIncludedService(Uuid uuid, int instanceId, int serviceType)
{
  mUuid = uuid;
  mInstanceId = instanceId;
  mServiceType = serviceType;
}

Uuid GattIncludedService::getUuid()
{
  return mUuid;
}

int GattIncludedService::getInstanceId()
{
  return mInstanceId;
}

int GattIncludedService::getType()
{
  return mServiceType;
}
}
