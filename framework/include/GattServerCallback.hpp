/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 * Not a Contribution.
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef GATTSERVERCALLBACK_HPP_
#define GATTSERVERCALLBACK_HPP_
#pragma once

#include "GattService.hpp"
#include "GattDescriptor.hpp"
#include "GattCharacteristic.hpp"
#include "common.h"

using namespace std;
using std::string;

namespace gatt{

/**
 * This abstract class is used to implement GattServer callbacks.
 */
class GattServerCallback {
  public:
    /**
     * Callback indicating when a remote device has been connected or disconnected.
     *
     * @param device Remote device that has been connected or disconnected.
     * @param status Status of the connect or disconnect operation.
     * @param newState Returns the new connection state. Can be one of
     * GattDevice#STATE_DISCONNECTED or GattDevice#STATE_CONNECTED
     */
    virtual void onConnectionStateChange(string UNUSED(deviceAddress), int UNUSED(status),
                                       int UNUSED(newState)){}

    /**
     * Indicates whether a local service has been added successfully.
     *
     * @param status Returns GattClient#GATT_SUCCESS if the service was added
     * successfully.
     * @param service The service that has been added
     */
    virtual void onServiceAdded(int UNUSED(status), GattService *UNUSED(service)){}

    /**
     * A remote client has requested to read a local characteristic.
     *
     * <p>An application must call GattServer#sendResponse
     * to complete the request.
     *
     * @param device The remote device that has requested the read operation
     * @param requestId The Id of the request
     * @param offset Offset into the value of the characteristic
     * @param characteristic Characteristic to be read
     */
    virtual void onCharacteristicReadRequest(string UNUSED(deviceAddress), int UNUSED(requestId),
                                            int UNUSED(offset), GattCharacteristic *UNUSED(characteristic)){}
    /**
     * A remote client has requested to write to a local characteristic.
     *
     * <p>An application must call GattServer#sendResponse
     * to complete the request.
     *
     * @param device The remote device that has requested the write operation
     * @param requestId The Id of the request
     * @param characteristic Characteristic to be written to.
     * @param preparedWrite true, if this write operation should be queued for later execution.
     * @param responseNeeded true, if the remote device requires a response
     * @param offset The offset given for the value
     * @param value The value the client wants to assign to the characteristic
     */
    virtual void onCharacteristicWriteRequest(string UNUSED(deviceAddress), int UNUSED(requestId),
                                              GattCharacteristic *UNUSED(characteristic),
                                              bool UNUSED(preparedWrite), bool UNUSED(responseNeeded),
                                              int UNUSED(offset), uint8_t *UNUSED(value), int UNUSED(length)){}

    /**
     * A remote client has requested to read a local descriptor.
     *
     * <p>An application must call GattServer#sendResponse
     * to complete the request.
     *
     * @param device The remote device that has requested the read operation
     * @param requestId The Id of the request
     * @param offset Offset into the value of the characteristic
     * @param descriptor Descriptor to be read
     */
    virtual void onDescriptorReadRequest(string UNUSED(deviceAddress), int UNUSED(requestId),
                                       int UNUSED(offset), GattDescriptor *UNUSED(descriptor)){}

    /**
     * A remote client has requested to write to a local descriptor.
     *
     * <p>An application must call GattServer#sendResponse
     * to complete the request.
     *
     * @param device The remote device that has requested the write operation
     * @param requestId The Id of the request
     * @param descriptor Descriptor to be written to.
     * @param preparedWrite true, if this write operation should be queued for later execution.
     * @param responseNeeded true, if the remote device requires a response
     * @param offset The offset given for the value
     * @param value The value the client wants to assign to the descriptor
     */
    virtual void onDescriptorWriteRequest(string UNUSED(deviceAddress), int UNUSED(requestId),
                                        GattDescriptor *UNUSED(descriptor),
                                        bool UNUSED(preparedWrite), bool UNUSED(responseNeeded),
                                        int UNUSED(offset), uint8_t *UNUSED(value), int UNUSED(length)){}
    /**
     * Execute all pending write operations for this device.
     *
     * <p>An application must call GattServer#sendResponse
     * to complete the request.
     *
     * @param device The remote device that has requested the write operations
     * @param requestId The Id of the request
     * @param execute Whether the pending writes should be executed (true) or cancelled (false)
     */
    virtual void onExecuteWrite(string UNUSED(deviceAddress), int UNUSED(requestId), bool UNUSED(execute)){}

    /**
     * Callback invoked when a notification or indication has been sent to
     * a remote device.
     *
     * <p>When multiple notifications are to be sent, an application must
     * wait for this callback to be received before sending additional
     * notifications.
     *
     * @param device The remote device the notification has been sent to
     * @param status GattClient#GATT_SUCCESS if the operation was successful
     */
    virtual void onNotificationSent(string UNUSED(deviceAddress), int UNUSED(status)){}

    /**
     * Callback indicating the MTU for a given device connection has changed.
     *
     * <p>This callback will be invoked if a remote client has requested to change
     * the MTU for a given connection.
     *
     * @param device The remote device that requested the MTU change
     * @param mtu The new MTU size
     */
    virtual void onMtuChanged(string UNUSED(deviceAddress), int UNUSED(mtu)){}

    /**
     * Callback triggered as result of GattServer#setPreferredPhy, or as a result
     * of remote device changing the PHY.
     *
     * @param device The remote device
     * @param txPhy the transmitter PHY in use. One of GattDevice#PHY_LE_1M,
     * GattDevice#PHY_LE_2M, and GattDevice#PHY_LE_CODED
     * @param rxPhy the receiver PHY in use. One of GattDevice#PHY_LE_1M,
     * GattDevice#PHY_LE_2M, and GattDevice#PHY_LE_CODED
     * @param status Status of the PHY update operation. GattClient#GATT_SUCCESS if the
     * operation succeeds.
     */
    virtual void onPhyUpdate(string UNUSED(deviceAddress), int UNUSED(txPhy), int UNUSED(rxPhy), int UNUSED(status)){}

    /**
     * Callback triggered as result of GattServer#readPhy
     *
     * @param device The remote device that requested the PHY read
     * @param txPhy the transmitter PHY in use. One of GattDevice#PHY_LE_1M,
     * GattDevice#PHY_LE_2M, and GattDevice#PHY_LE_CODED
     * @param rxPhy the receiver PHY in use. One of GattDevice#PHY_LE_1M,
     * GattDevice#PHY_LE_2M, and GattDevice#PHY_LE_CODED
     * @param status Status of the PHY read operation. GattClient#GATT_SUCCESS if the
     * operation succeeds.
     */
    virtual void onPhyRead(string UNUSED(deviceAddress), int UNUSED(txPhy), int UNUSED(rxPhy), int UNUSED(status)){}

    /**
     * Callback indicating the connection parameters were updated.
     *
     * @param device The remote device involved
     * @param interval Connection interval used on this connection, 1.25ms unit. Valid range is from
     * 6 (7.5ms) to 3200 (4000ms).
     * @param latency Slave latency for the connection in number of connection events. Valid range
     * is from 0 to 499
     * @param timeout Supervision timeout for this connection, in 10ms unit. Valid range is from 10
     * (0.1s) to 3200 (32s)
     * @param status GattClient#GATT_SUCCESS if the connection has been updated
     * successfully
     * @hide
     */
    virtual void onConnectionUpdated(string UNUSED(deviceAddress), int UNUSED(interval),
                                  int UNUSED(latency), int UNUSED(timeout), int UNUSED(status)){}

};
}
#endif
