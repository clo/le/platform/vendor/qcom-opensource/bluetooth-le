/*
 * Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.
 * Not a Contribution.
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef I_SCANNER_CALLBACK_HPP__
#define I_SCANNER_CALLBACK_HPP__

#pragma once
#include <iostream>
#include <vector>
using namespace std;
using std::string;

namespace gatt{
 /**
  *@hide
  */
class ScanResult;
class IScannerCallback {
  public:
    virtual void onScannerRegistered(int status, int scannerId) = 0;

    virtual void onScanResult(ScanResult *scanResult) = 0;
    virtual void onBatchScanResults(std::vector<ScanResult*> batchResults) = 0;
    virtual void onFoundOrLost(bool onFound, ScanResult *scanResult) = 0;
    virtual void onScanManagerErrorCallback(int errorCode) = 0;
};
}
#endif
