/*
Copyright (c) 2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef GATTLESCANNERMANAGER_HPP
#define GATTLESCANNERMANAGER_HPP

#include <systemdq/sd-bus.h>
#include <vector>

namespace gatt {
class ScanSettings;
class ScanFilter;
class ResultStorageDescriptor;
namespace ipc {
class GattDBusService;

class GattLeScannerManager final {
  public:
    explicit GattLeScannerManager(GattDBusService *dbus_service);
    ~GattLeScannerManager();
    bool InitialiseDBusService();

  private:
    static int sd_RegisterScanner(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int sd_UnregisterScanner(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int sd_StartScan(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int sd_StopScanByID(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int sd_StopScanByClient(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int sd_FlushPendingBatchResults(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int sd_DisconnectAll(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int sd_isScanClient(sd_bus *bus, const char *path, const char *interface, const char *property,
                                     sd_bus_message *reply, void *userdata, sd_bus_error *ret_error);

    int getScanSettingsFromMessage(sd_bus_message *m, ScanSettings **scanSettings);
    int getScanFiltersFromMessage(sd_bus_message *m, std::vector<ScanFilter*> *filters);
    int getStartScanParamsFromMessage(sd_bus_message *m, ScanSettings **scanSettings,
                                      std::vector<ScanFilter*> *filters,
                                      std::vector<std::vector<ResultStorageDescriptor*>> *storages);

    GattDBusService *gatt_dbus_service_;
    sd_bus_slot *sdbusSlot_ = nullptr;
};
} // namespace ipc
} // namespace gatt

#endif  // GATTLESCANNERMANAGER_HPP
